#!/bin/bash
# ------------  PROCESS ------------------------------------------------------------------------------

git submodule init
git submodule update

DIR=$(cd $(dirname $0); pwd)

## ディレクトリ検索効率化:percol
if [ -e ~/.percol.d ]; then
    echo "[Already installed ] percol";
else
    sudo pip install percol
fi


# ------------  CREATE SYMBOLIC LINK ------------------------------------------------------------------------------
# [bash]
ln -sf $DIR/bash/bashrc ~/.bashrc
ln -sf $DIR/bash/bash_profile ~/.bash_profile


# [zsh]
ln -sf $DIR/zsh/.antigen ~/.antigen
ln -sf $DIR/zsh/.antigen/repos/.zprezto ~/.zprezto
ln -sf $DIR/zsh/zshrc ~/.zshrc
ln -sf $DIR/zsh/zshenv ~/.zshenv
ln -sf $DIR/zsh/zlogin ~/.zlogin
ln -sf $DIR/zsh/zlogout ~/.zlogout
ln -sf $DIR/zsh/zpreztorc ~/.zpreztorc
ln -sf $DIR/zsh/zprofile ~/.zprofile

ln -sf $DIR/zsh/rc.py ~/.percol.d/rc.py


# [tmux]
ln -sf $DIR/tmux.conf ~/.tmux.conf
ln -sf $DIR/tmux ~/tmux
sudo ln -sf $DIR/bin/get_battery /usr/local/bin/get_battery
brew install reattach-to-user-namespace


# [Vim]
ln -sf $DIR/.vim ~/.vim
ln -sf $DIR/.vim/vimrc ~/.vimrc


# [git]
ln -sf $DIR/gitconfig ~/.gitconfig


# ------------  READ FILES  ------------------------------------------------------------------------------
# [PROCESS:2] dotfilesの設定を反映
~/.bashrc
~/.bash_profile
~/.zshrc
~/.vimrc
tmux source-file ~/.tmux.conf
echo [使うSHELLは自分で切り替えろ]


