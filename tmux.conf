#-------- BASIC SETTTING ----------------------------------------------- 
# 親端末のTERMがxtermの場合
set -g terminal-overrides 'xterm*:smcup@:rmcup@'

set -sg escape-time 0 

# マウス操作を有効にする
setw -g mode-mouse on
set -g mouse-select-pane on
set -g mouse-resize-pane on
set -g mouse-select-window on

# 256色端末を使用する
set -g default-terminal "screen-256color"


#--------COLOURS (Solarized dark) ----------------------------------------------- 
# default statusbar colors
set -g status-bg black #base02
set -g status-fg yellow #yellow
set -g status-attr default

# ウィンドウリストの色を設定する
setw -g window-status-fg cyan
setw -g window-status-bg default
setw -g window-status-attr dim
# アクティブなウィンドウを目立たせる
setw -g window-status-current-fg white #orange
setw -g window-status-current-bg brightred
setw -g window-status-current-attr bright

# ペインボーダーの色を設定する
set -g pane-border-fg cyan
set -g pane-border-bg black

# アクティブなペインを目立たせる
set -g pane-active-border-fg white
set -g pane-active-border-bg cyan

# ウィンドウ履歴の最大行数
set -g history-limit 5000

# message text
set -g message-bg green #base02
set -g message-fg white #orange
set -g message-attr bright

# pane number display
set -g display-panes-active-colour blue #blue
set -g display-panes-colour brightred #orange

# clock
setw -g clock-mode-colour green #green


#------- GUI -------------------------------------------------------------------- 
# ウィンドウのインデックスを1から始める
set -g base-index 1

# ペインのインデックスを1から始める
setw -g pane-base-index 1

# ステータスバーを設定する
## 左右のステータスバーの長さを決定する
set -g status-left-length 90
set -g status-right-length 90

## ステータスバーのUTF-8サポートを有効にする
set -g status-utf8 on

## リフレッシュの間隔を設定する(デフォルト 15秒)
set -g status-interval 60

## センタライズ（主にウィンドウ番号など）
set -g status-justify centre

## 最左に表示：H => マシン名 P => ペイン番号
set -g status-left '#H:[#P]'

## 最右に表示：Wi-Fi、バッテリー残量、現在時刻
set -g status-right '#(/usr/local/bin/get_battery -c tmux) [%Y-%m-%d(%a) %H:%M]'

## ヴィジュアルノーティフィケーションを有効にする
setw -g monitor-activity on
set -g visual-activity on

## ステータスバーを上部に表示する
set -g status-position top



#------- key bind  --------------------------------------------------------------------
# prefixキーをC-jに変更
set -g prefix C-j

# C-bのキーバインドを解除
unbind C-b


# 設定ファイルをリロードする
bind r source-file ~/.tmux.conf \; display "Reloaded!"


# ペイン
## | でペインを縦に分割する
bind | split-window -h
## - でペインを横に分割する
bind - split-window -v
## Vimのキーバインドでペインを移動する
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+
## Vimのキーバインドでペインをリサイズ
bind -r H resize-pane -L 10
bind -r J resize-pane -D 10
bind -r K resize-pane -U 10
bind -r L resize-pane -R 10
##ペインの破棄
#bind C-K confirm-before "kill-pane"
## Ctrl-o でペインをローテーションしながら移動。Prefix を用いないのでタイプが楽だが、Ctrl-o を使用してしまう。他のソフトウェアの設定に支障をきたさないように注意
bind-key -n C-o select-pane -t :.+


# ウィンドウ
## 移動
bind n next-window
bind p previous-window
bind c new-window
## 破棄
bind K confirm-before "kill-window"


#-------- COPY MODE ----------------------------------------------- 
# viのキーバインドを使用する
setw -g mode-keys vi

# クリップボード共有を有効にする
set-option -g default-command "reattach-to-user-namespace -l zsh"

# コピーモードの操作をvi風に設定する
bind-key v copy-mode \; display "Copy mode!"
bind-key -t vi-copy v begin-selection
bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"
unbind -t vi-copy Enter
bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"
# コピーモード中（Prefix+v 後）C-v で矩形選択開始
bind-key -t vi-copy C-v rectangle-toggle
# 1行選択
bind-key -t vi-copy V select-line
# Vi モード中に Ctrl-a で行頭に（Emacs ライク）
bind-key -t vi-copy C-a start-of-line
# Vi モード中に Ctrl-e で行末に（Emacs ライク）
bind-key -t vi-copy C-e end-of-line

# 単語の最初の1文字に移動
bind-key -t vi-copy w next-word
# 単語の最後の1文字に移動
bind-key -t vi-copy e next-word-end
# w の逆の動き back
bind-key -t vi-copy b previous-word

# 画面上に映る最上行に移動
bind-key -t vi-copy g top-line
# 画面上に映る最下行に移動
bind-key -t vi-copy G bottom-line

# 前方検索
bind-key -t vi-copy / search-forward
# 後方検索
bind-key -t vi-copy ? search-backward

# ページスクロール
bind-key -t vi-copy C-n page-up
bind-key -t vi-copy C-f page-down
# ページ送り
bind-key -t vi-copy C-u scroll-up
bind-key -t vi-copy C-d scroll-down
